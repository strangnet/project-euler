#!/usr/bin/env ruby

# 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
# What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

class Integer
  def factors()
    y = self
    i = 2
    arr = []

    until y == 1
      q, r = y.divmod i
      if r != 0
        i += i > 2 ? 2 : 1 # no need to test multiples of 2
      else
        arr << i
        y = q
      end
    end

    arr
  end
end

def factorize(range)
  # collect all factors
  factors = []
  #iterate through the range
  range.each do |n|
    next if n < 2
    f = n.factors
    f.uniq.each do |a|
      # fill upp the gathered factors to match the minimum required
      # by one element in the range
      if f.count(a) > factors.count(a)
        (f.count(a) - factors.count(a)).times { factors << a }
      end
    end
  end
  factors.sort
end

time = Time.now
lqq = factorize(1..10).reduce(1, :*)
time = (Time.now - time) * 1000
p "1..10: #{lqq} (#{time} ms)"

time = Time.now
lqq = factorize(1..20).reduce(1, :*)
time = (Time.now - time) * 1000
p "1..20: #{lqq} (#{time} ms)"