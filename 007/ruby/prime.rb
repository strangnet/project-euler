#!/usr/bin/env ruby

# By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
# What is the 10 001st prime number?

def find_nth_prime(n)
  primes = [2]
  current = 3
  until primes.length == n
    found = false
    primes.each do |p|
      break if p > Math.sqrt(current).to_i
      found = true if current % p == 0
      break if found
    end
    primes << current if not found
    current += 2
  end
  primes.last
end

n = 10001
start = Time.now
pr = find_nth_prime(n)
stop = Time.now - start

p "#{n}: #{pr} (#{stop*1000} ms)"