#!/usr/bin/env ruby

# The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
# Find the sum of all the primes below two million.

def find_primes_upto(n)
  primes = [2]
  current = 3
  while current < n
    found = false
    primes.each do |p|
      break if p > Math.sqrt(current).to_i
      found = true if current % p == 0
      break if found
    end
    primes << current if not found
    current += 2
  end
  primes
end

n = 2000000
start = Time.now
pr = find_primes_upto(n)
sum = pr.inject(:+)
stop = Time.now - start

p "#{sum} in #{stop*1000} ms"