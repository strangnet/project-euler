#!/usr/bin/env ruby

# The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
# Find the sum of all the primes below two million.

def find_primes_upto(n)
  primes = [nil, nil] + (2..n).to_a
  (2..Math.sqrt(n).to_i).each do |i|
    if not primes[i].nil?
      (i**2..n).step(i) {|m| primes[m] = nil}
    end
  end
  primes.compact
end

n = 2000000
start = Time.now
pr = find_primes_upto(n)
sum = pr.inject(:+)
stop = Time.now - start

p "#{sum} in #{stop*1000} ms"