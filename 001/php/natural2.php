<?php

function divisable($max, $num)
{
  $top = floor(($max - 1) / $num);
  return $num * ($top / 2) * ($top + 1);
}

$sum = divisable(1000, 3) + divisable(1000, 5) - divisable(1000, 15);
echo $sum . "\n";