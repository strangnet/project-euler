#!/bin/ruby

# divisable(20, 3)
# 3 + 6 + 9 + 12 + 15 + 18 <=> 3 * (1 + 2 + 3 + 4 + 5 + 6)
# n = (20 - 1) / 3 = 6
# 1 + 2 + 3 + 4 + ... + n <=> (n * (n + 1)) / 2

def divisable(max_num, divisor)
  n = (max_num - 1) / divisor 
  divisor * n * (n + 1) / 2
end

sum = divisable(1000, 3) + divisable(1000, 5) - divisable(1000, 15)

puts "Sum: #{sum}"
