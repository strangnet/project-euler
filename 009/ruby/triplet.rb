#!/usr/bin/env ruby

# A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

# a^2 + b^2 = c^2
# For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

# There exists exactly one Pythagorean triplet for which a + b + c = 1000.
# Find the product abc.

# ============
# We know that a < b < c, and that (a + b) > c

sum = 1000

t1 = Time.now
triplet = product = nil

1.upto(sum/3) do |a|
  (a+1).upto(sum/2) do |b|
    c = 1000 - b - a
    if c > b and c > a and c < (a + b) and (c**2 == a**2 + b**2)
      triplet = "#{a} #{b} #{c}"
      product = a*b*c
    end
    break if product
  end
  break if product
end
t2 = Time.now

p "Triplet: #{triplet}"
p "Product: #{product}"
p "#{(t2-t1)*1000} ms"