#!/bin/ruby

# By considering the terms in the Fibonacci sequence whose values do not exceed four million, 
# find the sum of the even-valued terms.

# 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144

def time_method(method, args)
  start_time = Time.now
  self.send(method, args)
  end_time = Time.now
  puts "Time elapsed: #{(end_time - start_time) * 1000} msec"
end

def fib(n)
  n < 2 ? n : fib(n-2) + fib(n-1)
end

def fib_iter(limit)
  cur = 1
  nxt = 2
  sum = 0

  while cur < limit
    if cur.even?
      sum += cur
    end
    cur, nxt = nxt, cur + nxt
  end

  puts sum
end

time_method(:fib_iter, 4000000)