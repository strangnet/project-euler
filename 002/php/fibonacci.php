<?php

function microtime_float()
{
  list($usec, $sec) = explode(" ", microtime());
  return ((float)$usec + (float)$sec);
}

function fib($num, &$vals)
{
  if (!key_exists($num, $vals)) {
    $vals[$num] = in_array($num, array(1,2)) ? $num : fib($num - 2, $vals) + fib($num - 1, $vals);
  }
  return $vals[$num];
}

$alt = 1;

$sum = 0;
$time = microtime_float();

if ($alt == 1) {
  $num = 1;
  $vals = array();
  do {
    fib($num, $vals);
    $num++;
  } while ($vals[$num - 1] < 4000000);
  
  foreach ($vals as $val) {
    if ($val % 2 == 0) {
      $sum += $val;
    }
  }
} else if ($alt == 2) {
  $max = 4000000;
  $a = 1;
  $b = 1;
  while ($b < $max) {
    if ($b % 2 == 0) $sum += $b;
    $h = $a + $b;
    $a = $b;
    $b = $h;
  }
} else if ($alt == 3) {
  $max = 4000000;
  $a = 1;
  $b = 1;
  $c = $a + $b;
  while ($c < $max) {
    $sum += $c;
    $a = $c + $b;
    $b = $c + $a;
    $c = $a + $b;
  }
}

echo $sum . " (" . (microtime_float() - $time) * 1000 . " msec)\n";