#!/usr/bin/env ruby

# The prime factors of 13195 are 5, 7, 13 and 29.
# What is the largest prime factor of the number 600851475143 ?

class Integer
  def factors()
    y = self
    i = 2
    arr = []

    until y == 1
      q, r = y.divmod i
      if r != 0
        i += i > 2 ? 2 : 1 # no need to test multiples of 2
      else
        arr << i
        y = q
      end
    end

    arr.uniq
  end
end

fct = []
time = []

start_time = Time.now
fct << 13195.factors
time << (Time.now - start_time) * 1000

start_time = Time.now
fct << 600851475143.factors
time << (Time.now - start_time) * 1000

p "13195: #{fct[0]}, max: #{fct[0].each_with_index.max[0]}, (#{time[0]} ms)"
p "600851475143: #{fct[1]}, max: #{fct[1].each_with_index.max[0]}, (#{time[1]} ms)"
