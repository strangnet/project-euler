#!/bin/ruby

# A palindromic number reads the same both ways. 
# The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 99.
# Find the largest palindrome made from the product of two 3-digit numbers.

start_time = Time.now

largest = 0
(100..999).each do |a|
  (a...999).each do |b|
    s = a * b
    largest = [largest, s].max if s.to_s == s.to_s.reverse
  end
end

end_time = Time.now

p largest
p (end_time - start_time) * 1000